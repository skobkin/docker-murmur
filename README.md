# Moved to Forgejo

Latest version is available here: https://git.skobk.in/skobkin/docker-murmur

# Murmur server docker image

## Build

```shell
# For version 1.4.230
docker build --build-arg VERSION=1.4.230 --tag 'skobkin/murmur:1.4.230' --tag 'skobkin/murmur:latest' .
```

## Publish

```shell
docker push skobkin/murmur:latest skobkin/murmur:1.4.230
```

## Usage

See [skobkin/docker-stacks](https://bitbucket.org/skobkin/docker-stacks/src/master/murmur/).
